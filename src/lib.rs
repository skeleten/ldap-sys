#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(clippy::unreadable_literal)]
#![allow(clippy::redundant_static_lifetimes)]

use std::env;

use std::os::raw::{c_char};

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[cfg(feature = "deprecated")]
mod deprecated {
	use crate::*;
	use std::os::raw::{c_int, c_void};

	// Functions (might be deprecated)
	extern "C" {
		#[deprecated(note = "Use `ldap_control_create` instead")]
		pub fn ldap_create_control(
			requestOID: *const c_char,
			ber: *mut BerElement,
			iscritical: c_int,
			ctrlp: *mut *mut LDAPControl,
		) -> c_int;
		#[deprecated(note = "Use `ldap_control_find` instead")]
		pub fn ldap_find_control(oid: *const c_char, ctrls: *mut *mut LDAPControl) -> c_int;

		#[deprecated(note = "Use `ldap_abandon_ext` instead")]
		pub fn ldap_abandon(ld: *mut LDAP, msg_id: c_int) -> c_int;

		#[deprecated(note = "Use `ldap_add_ext` instead")]
		pub fn ldap_add(ld: *mut LDAP, dn: *const c_char, attrs: *mut *mut LDAPMod) -> c_int;
		#[deprecated(note = "USE `ldap_add_ext_s` instead")]
		pub fn ldap_add_s(ld: *mut LDAP, dn: *const c_char, attrs: *mut *mut LDAPMod) -> c_int;

		#[deprecated(note = "Use `ldap_sasl_bind` instead")]
		pub fn ldap_bind(
			ld: *mut LDAP,
			who: *const c_char,
			passwd: *const c_char,
			authmethod: c_int,
		) -> c_int;
		#[deprecated(note = "Use `ldap_sasl_bind_s` instead")]
		pub fn ldap_bind_s(
			ld: *mut LDAP,
			who: *const c_char,
			passwd: *const c_char,
			authmethod: c_int,
		) -> c_int;
		#[deprecated(note = "Use `ldap_sasl_bind` instead")]
		pub fn ldap_simple_bind(ld: *mut LDAP, who: *const c_char, passwd: *const c_char) -> c_int;
		#[deprecated(note = "Use `ldap_sasl_bind_s` instead")]
		pub fn ldap_simple_bind_s(ld: *mut LDAP, who: *const c_char, passwd: *const c_char) -> c_int;

		#[deprecated(note = "Use `ldap_compare_ext` instead")]
		pub fn ldap_compare(
			ld: *mut LDAP,
			dn: *const c_char,
			attr: *const c_char,
			value: *const c_char,
		) -> c_int;
		#[deprecated(note = "Use `ldap_compare_ext_s` instead")]
		pub fn ldap_compare_s(
			ld: *mut LDAP,
			dn: *const c_char,
			attr: *const c_char,
			value: *const c_char,
		) -> c_int;

		#[deprecated(note = "Use `ldap_delete_ext` instead")]
		pub fn ldap_delete(ld: *mut LDAP, dn: *const c_char) -> c_int;
		#[deprecated(note = "Use `ldap_delete_ext_s` instead")]
		pub fn ldap_delete_s(ld: *mut LDAP, dn: *const c_char) -> c_int;

		#[deprecated(note = "Use `ldap_parse_result` instead")]
		pub fn ldap_result2error(ld: *mut LDAP, r: *mut LDAPMessage, freeit: c_int) -> c_int;
		#[deprecated(note = "Use `ldap_err2string` instead")]
		pub fn ldap_perror(ld: *mut LDAP, s: *const c_char) -> c_void;

		#[deprecated(note = "Use `ldap_modify_ext` instead")]
		pub fn ldap_modify(ld: *mut LDAP, dn: *const c_char, mods: *mut *mut LDAPMod) -> c_int;
		#[deprecated(note = "Use `ldap_modify_ext_s` instead")]
		pub fn ldap_modify_s(ld: *mut LDAP, dn: *const c_char, mods: *mut *mut LDAPMod) -> c_int;

		#[deprecated(note = "Use `ldap_rename` instead")]
		pub fn ldap_rename2(
			ld: *mut LDAP,
			dn: *const c_char,
			newrdn: *const c_char,
			new_superior: *const c_char,
			deleteoldrdn: c_int,
		) -> c_int;
		#[deprecated(note = "Use `ldap_rename_s` instead")]
		pub fn ldap_rename2_s(
			ld: *mut LDAP,
			dn: *const c_char,
			newrdn: *const c_char,
			newSuperior: *const c_char,
			deleteoldrdn: c_int,
		) -> c_int;
		#[deprecated(note = "Use `ldap_rename` instead")]
		pub fn ldap_modrdn(ld: *mut LDAP, dn: *const c_char, newrdn: *const c_char) -> c_int;
		#[deprecated(note = "Use `ldap_rename_s` instead")]
		pub fn ldap_modrdn_s(ld: *mut LDAP, dn: *const c_char, newrdn: *const c_char) -> c_int;
		#[deprecated(note = "Use `ldap_rename` instead")]
		pub fn ldap_modrdn2(
			ld: *mut LDAP,
			dn: *const c_char,
			newrdn: *const c_char,
			deleteoldrdn: c_int,
		) -> c_int;
		#[deprecated(note = "Use `ldap_rename_s` instead")]
		pub fn ldap_modrdn2_s(
			ld: *mut LDAP,
			dn: *const c_char,
			newrdn: *const c_char,
			deleteoldrdn: c_int,
		) -> c_int;

		#[deprecated(note = "Use `ldap_initialize`")]
		pub fn ldap_init(host: *const c_char, port: c_int) -> *mut LDAP;
		#[deprecated(note = "Use `ldap_initialize`")]
		pub fn ldap_open(host: *const c_char, port: c_int) -> *mut LDAP;

		#[deprecated(note = "Use `ldap_get_values_len` instead")]
		pub fn ldap_get_values(
			ld: *mut LDAP,
			entry: *mut LDAPMessage,
			target: *const c_char,
		) -> *mut *mut c_char;
		#[deprecated(note = "Use `ldap_count_values_len` instead")]
		pub fn ldap_count_values(vals: *mut *mut c_char) -> c_int;

		#[deprecated(note = "Use `ldap-value_free_len` instead")]
		pub fn ldap_value_free(vals: *mut *mut c_char);

		#[deprecated(note = "Use `ldap_search_ext` instead")]
		pub fn ldap_search(
			ld: *mut LDAP,
			base: *const c_char,
			scope: c_int,
			filter: *const c_char,
			attrs: *mut *mut c_char,
			attrsonly: c_int,
		) -> c_int;
		#[deprecated(note = "Use `ldap_search_ext_s` instead")]
		pub fn ldap_search_s(
			ld: *mut LDAP,
			base: *const c_char,
			scope: c_int,
			filter: *const c_char,
			attrs: *mut *mut c_char,
			attrsonly: c_int,
			res: *mut *mut LDAPMessage,
		) -> c_int;
		#[deprecated(note = "Use `ldap_search_ext_s` instead")]
		pub fn ldap_search_st(
			ld: *mut LDAP,
			base: *const c_char,
			scope: c_int,
			filter: *const c_char,
			attrs: *mut *mut c_char,
			attrsonly: c_int,
			timeout: *const timeval,
			res: *mut *mut LDAPMessage,
		) -> c_int;

		#[deprecated(note = "Use `ldap_unbind_ext` instead")]
		pub fn ldap_unbind(ld: *mut LDAP) -> c_int;
		#[deprecated(note = "Use `ldap_unbind_ext_s` instead")]
		pub fn ldap_unbind_s(ld: *mut LDAP) -> c_int;

		#[deprecated(note = "Use `ldap_parse_pageresponse_control` instead")]
		pub fn ldap_parse_page_control(ld: *mut LDAP, ctrls: *mut *mut LDAPControl, count: *mut ber_int_t, cookie: *mut *mut berval);
	}

	#[deprecated]
	pub type LDAP_SORT_AD_CMP_PROC = extern "C" fn(left: *const c_char, right: *const c_char) -> c_int;
	#[deprecated]
	pub type LDAP_SORT_AV_CMP_PROC = extern "C" fn(left: *const c_void, right: *const c_void) -> c_int;

	extern "C" {
		#[allow(deprecated)]
		#[deprecated]
		pub fn ldap_sort_entries(ld: *mut LDAP, vals: *mut *mut c_char, cmp: *mut LDAP_SORT_AV_CMP_PROC) -> c_int;
	}
}

#[cfg(feature = "deprecated")]
pub use crate::deprecated::*;

// Constants and methods BINDGEN does not seem to care about

pub const LDAP_TAG_SYNC_NEW_COOKIE: ber_tag_t = 0x80;
pub const LDAP_TAG_SYNC_REFRESH_DELETE: ber_tag_t = 0xa1;
pub const LDAP_TAG_SYNC_REFRESH_PRESENT: ber_tag_t = 0xa2;
pub const LDAP_TAG_SYNC_ID_SET: ber_tag_t = 0xa3;

pub const LDAP_TAG_SYNC_COOKIE: ber_tag_t = 0x04;
pub const LDAP_TAG_REFRESHDELETES: ber_tag_t = 0x01;
pub const LDAP_TAG_REFERESHDONE: ber_tag_t = 0x01;
pub const LDAP_TAG_RELOAD_HINT: ber_tag_t = 0x01;

pub const LDAP_TAG_EXOP_MODIFY_PASSWD_ID: ber_tag_t = 0x80;
pub const LDAP_TAG_EXOP_MODIFY_PASSWD_OLD: ber_tag_t = 0x81;
pub const LDAP_TAG_EXOP_MODIFY_PASSWD_NEW: ber_tag_t = 0x82;
pub const LDAP_TAG_EXOP_MODIFY_PASSWD_GEN: ber_tag_t = 0x80;

pub const LDAP_TAG_EXOP_REFRESH_REQ_DN: ber_tag_t = 0x80;
pub const LDAP_TAG_EXOP_REFRESH_REQ_TTL: ber_tag_t = 0x81;
pub const LDAP_TAG_EXOP_REFRESH_RES_TTL: ber_tag_t = 0x81;

pub const LDAP_TAG_MESSAGE: ber_tag_t = 0x30;
pub const LDAP_TAG_MSGID: ber_tag_t = 0x02;
pub const LDAP_TAG_LDAPDN: ber_tag_t = 0x04;
pub const LDAP_TAG_LDAPCRED: ber_tag_t = 0x04;

pub const LDAP_TAG_CONTROLS: ber_tag_t = 0xa0;
pub const LDAP_TAG_REFERRAL: ber_tag_t = 0xa3;

pub const LDAP_TAG_NEWSUPERIOR: ber_tag_t = 0x80;

pub const LDAP_TAG_EXOP_REQ_OID: ber_tag_t = 0x80;
pub const LDAP_TAG_EXOP_REQ_VALUE: ber_tag_t = 0x81;
pub const LDAP_TAG_EXOP_RES_OID: ber_tag_t = 0x8a;
pub const LDAP_TAG_EXOP_RES_VALUE: ber_tag_t = 0x8b;

pub const LDAP_TAG_IM_RES_OID: ber_tag_t = 0x80;
pub const LDAP_TAG_IM_RES_VALUE: ber_tag_t = 0x81;

pub const LDAP_TAG_SASL_RES_CREDS: ber_tag_t = 0x87;

// LDAP Request Message
pub const LDAP_REQ_BIND: ber_tag_t = 0x60;
pub const LDAP_REQ_UNBIND: ber_tag_t = 0x42;
pub const LDAP_REQ_SEARCH: ber_tag_t = 0x63;
pub const LDAP_REQ_MODIFY: ber_tag_t = 0x66;
pub const LDAP_REQ_ADD: ber_tag_t = 0x68;
pub const LDAP_REQ_DELETE: ber_tag_t = 0x4a;
pub const LDAP_REQ_MODDN: ber_tag_t = 0x6c;
pub const LDAP_REQ_MODRDN: ber_tag_t = LDAP_REQ_MODDN;
pub const LDAP_REQ_RENAME: ber_tag_t = LDAP_REQ_MODDN;
pub const LDAP_REQ_COMPARE: ber_tag_t = 0x6e;
pub const LDAP_REQ_ABANDON: ber_tag_t = 0x50;
pub const LDAP_REQ_EXTENDED: ber_tag_t = 0x77;

// LDAP Response Messages
pub const LDAP_RES_BIND: ber_tag_t = 0x61;
pub const LDAP_RES_SEARCH_ENTRY: ber_tag_t = 0x64;
pub const LDAP_RES_SEARCH_REFERENCE: ber_tag_t = 0x73;
pub const LDAP_RES_SEARCH_RESULT: ber_tag_t = 0x65;
pub const LDAP_RES_MODIFY: ber_tag_t = 0x67;
pub const LDAP_RES_ADD: ber_tag_t = 0x69;
pub const LDAP_RES_DELETE: ber_tag_t = 0x6b;
pub const LDAP_RES_MODDN: ber_tag_t = 0x6d;
pub const LDAP_RES_MODRDN: ber_tag_t = LDAP_RES_MODDN;
pub const LDAP_RES_RENAME: ber_tag_t = LDAP_RES_MODDN;
pub const LDAP_RES_COMPARE: ber_tag_t = 0x6f;
pub const LDAP_RES_EXTENDED: ber_tag_t = 0x78;
pub const LDAP_RES_INTERMEDIATE: ber_tag_t = 0x79;

// sasl methods
pub const LDAP_SASL_SIMPLE: *const c_char = std::ptr::null();

// authentication methods available
pub const LDAP_AUTH_NONE: ber_tag_t = 0x00;
pub const LDAP_AUTH_SIMPLE: ber_tag_t = 0x80;
pub const LDAP_AUTH_SASL: ber_tag_t = 0xa3;
pub const LDAP_AUTH_KRBV4: ber_tag_t = 0xff;
pub const LDAP_AUTH_KRBV41: ber_tag_t = 0x81;
pub const LDAP_AUTH_KRBV42: ber_tag_t = 0x82;

/// Used by the Windows API but not used on the wire
pub const LDAP_AUTH_NEGOTIATE: ber_tag_t = 0x04FF;

// Filter types
pub const LDAP_FILTER_AND: ber_tag_t = 0xa0;
pub const LDAP_FILTER_OR: ber_tag_t = 0xa1;
pub const LADP_FILTER_NOT: ber_tag_t = 0xa2;
pub const LDAP_FILTER_EQUALITY: ber_tag_t = 0xa3;
pub const LDAP_FILTER_SUBSTRINGS: ber_tag_t = 0xa4;
pub const LDAP_FILTER_GE: ber_tag_t = 0xa5;
pub const LDAP_FILTER_LE: ber_tag_t = 0xa6;
pub const LDAP_FILTER_PRESENT: ber_tag_t = 0x87;
pub const LDAP_FILTER_APPROX: ber_tag_t = 0xa8;
pub const LDAP_FILTER_EXT: ber_tag_t = 0xa9;

// Extended filter component types
pub const LDAP_FILTER_EXT_OID: ber_tag_t = 0x81;
pub const LDAP_FILTER_EXT_TYPE: ber_tag_t = 0x82;
pub const LDAP_FILTER_EXT_VALUE: ber_tag_t = 0x83;
pub const LDAP_FILTER_EXT_DNATTRS: ber_tag_t = 0x84;

// Substring filter component types
pub const LDAP_SUBSTRING_INITIAL: ber_tag_t = 0x80;
pub const LDAP_SUBSTRING_ANY: ber_tag_t = 0x81;
pub const LDAP_SUBSTRING_FINAL: ber_tag_t = 0x82;

// Search scopes
pub const LDAP_SCOPE_BASE: ber_int_t = 0x0000;
pub const LDAP_SCOPE_BASEOBJECT: ber_int_t = LDAP_SCOPE_BASE;
pub const LDAP_SCOPE_ONELEVEL: ber_int_t = 0x0001;
pub const LDAP_SCOPE_ONE: ber_int_t = LDAP_SCOPE_ONELEVEL;
pub const LDAP_SCOPE_SUBTREE: ber_int_t = 0x0002;
pub const LDAP_SCOPE_SUB: ber_int_t = LDAP_SCOPE_SUBTREE;
pub const LDAP_SCOPE_SUBORDINATE: ber_int_t = 0x0003; // OpenLDAP Extension
pub const LDAP_SCOPE_CHILDREN: ber_int_t = LDAP_SCOPE_SUBORDINATE;
pub const LDAP_SCOPE_DEFAULT: ber_int_t = -1; // OpenLDAP Extension
